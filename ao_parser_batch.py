#!/usr/bin/env python2
from __future__ import generators
import os
import json
import fnmatch
import re
import pprint
import subprocess
from datetime import datetime as dt
from datetime import timedelta
from ao_parser_node import Srn_Node
import thread
from multiprocessing import Process
class Batch_Session():
	'''
	Initilization: 
	@param: root directory of batch mode session.
	1. Setup apporiate path where the files are located
	2. Load match config JSON and make data available in program runtime
	3. Instantiate SRN node class which will start parsing traffc log drc.
	'''
	def __init__(self, match_dir):
		self._session_dir_name = match_dir
		self._traffic_log_path = self._session_dir_name+'/traffic_logs'

		self._json_data_batch = open(self._session_dir_name + '/Inputs/batch_input.json').read()
		self._data_batch = json.loads(self._json_data_batch)

		self._rf_scenario = self._data_batch["RFScenario"]
		self._traffic_scenario = self._data_batch["TrafficScenario"]
		self._batch_name = self._data_batch["BatchName"]
		self._batch_name = self._batch_name.replace(" ","_")
		self._json_mapping_data = open(self._session_dir_name+'/Inputs/match_conf.json').read()
		self._mapping_data = json.loads(self._json_mapping_data)
		self._node_to_srn_mapping = self._mapping_data['node_to_srn_mapping']

		self._json_node_data = self._data_batch['NodeData']
		self._global_start_time_obj = self.get_global_start_ts()
		self._global_stop_time_obj = self.get_global_stop_ts()
		self._node_list = []

		for node in self._json_node_data:
				node_instance = Srn_Node(node['RFNode_ID'], self._node_to_srn_mapping[str(node['RFNode_ID'])],self._node_to_srn_mapping, 0,self._rf_scenario,self._traffic_scenario,self._traffic_log_path,self._global_start_time_obj,self._global_stop_time_obj,"BATCH")
				node_instance.upload_to_cloud()

		#print("\n\t=======DRC Parsing COMPLETED=======\n")
	

	def get_global_start_ts(self):
		p_grep = subprocess.Popen(['grep','-rw',self._traffic_log_path, "-e", "START Mgen"], stdout=subprocess.PIPE)
		p_cut1 = subprocess.Popen(["cut","-d",":","-f","2-"],stdin=p_grep.stdout, stdout=subprocess.PIPE)
		p_cut2 = subprocess.Popen(["cut","-d"," ","-f","1"], stdin=p_cut1.stdout, stdout=subprocess.PIPE)
		out, err = p_cut2.communicate()
		ts_list = out.splitlines()
		ts_obj_list = []
		for ts in ts_list:
			ts_obj_list.append( dt.strptime(ts, '%Y-%m-%d_%H:%M:%S.%f'))
		return min(ts_obj_list)

	def get_global_stop_ts(self):
		p_grep = subprocess.Popen(['grep','-rw',self._traffic_log_path, "-e", "STOP"], stdout=subprocess.PIPE)
		p_cut1 = subprocess.Popen(["cut","-d",":","-f","2-"],stdin=p_grep.stdout, stdout=subprocess.PIPE)
		p_cut2 = subprocess.Popen(["cut","-d"," ","-f","1"], stdin=p_cut1.stdout, stdout=subprocess.PIPE)
		out, err = p_cut2.communicate()
		ts_list = out.splitlines()
		ts_obj_list = []
		for ts in ts_list:
			ts_obj_list.append( dt.strptime(ts, '%Y-%m-%d_%H:%M:%S.%f'))
		return max(ts_obj_list)

def make_session(path):
	try:
		Batch_Session(path)
	except Exception as e:
		print(e)


if __name__ == "__main__":
		ts_now = dt.now()
		ts_offset = timedelta(days=1)#Number of days to consider match valid
		ts_cutoff = ts_now - ts_offset
		for reservation_path in os.listdir("/root/colosseum-nas/"):
			if fnmatch.fnmatch(reservation_path, "RESERVATION-*"):
				dir_stat = os.stat("/root/colosseum-nas/"+reservation_path)
				ts_dir_last_changed = dt.fromtimestamp(dir_stat.st_ctime)
				if ts_dir_last_changed > ts_cutoff:
					abs_reservation_path = "/root/colosseum-nas/" + reservation_path
					p = Process(target=make_session, args=(abs_reservation_path,))
					p.start()
					print("Firing up process for {}".format(reservation_path))
		p.join()
