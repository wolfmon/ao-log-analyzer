#!/usr/bin/env python2
from __future__ import generators
import os
import json
import fnmatch
import re
import pprint
import subprocess
from datetime import datetime as dt
from datetime import timedelta
from multiprocessing import Process
from ao_parser_node import Srn_Node
import thread

class Freeplay_Session():
	def __init__(self, match_dir):
		self._session_dir_name = match_dir
		self._traffic_log_path = self._session_dir_name+'/traffic_logs'

		self._json_data_freeplay = open(self._session_dir_name + '/Inputs/freeplay.json').read()
		self._data_freeplay = json.loads(self._json_data_freeplay) 

		self._rf_scenario = self._data_freeplay["RFScenario"]
		self._traffic_scenario = self._data_freeplay["TrafficScenario"]
		self._node_to_srn_mapping = {(str):(int)}
		self._json_node_data = self._data_freeplay["NodeData"]
		self._team_number = -1
		self._fuse_node_id_found = False
		self._one_of_our_node = "NULL"
		self._gw_node_id = "NULL"
		self._global_start_time_obj = self.get_global_start_ts()
		self._global_stop_time_obj = self.get_global_stop_ts()
		#print("GTS:",self._global_start_time_obj)
		
		
		for name in os.listdir(self._session_dir_name):

			if fnmatch.fnmatch(name, "freeplay-srn*") and os.path.isdir(self._session_dir_name +"/" + name) and not self._fuse_node_id_found:
				self._one_of_our_node = re.split('-srn|-', name)[1]
				self._fuse_node_id_found = True
				#print("found one of our node ", self._one_of_our_node)
		
		for node in self._json_node_data:
			self._node_to_srn_mapping.update({str(node["RFNode_ID"]):node["srn_number"]})			
			if str(node["srn_number"]) == str(self._one_of_our_node):
				self._team_number = node["team_no"]

		#pprint.pprint(self._node_to_srn_mapping)
		#print("TeamNum", self._team_number)
		for node in self._json_node_data:
			if str(self._team_number) == str(node["team_no"]):
				if(str(node['isGateway']) == str(True)):
					#print("Gateway!!!")
					self._gw_node_id = node['RFNode_ID']
				node_instance = Srn_Node(node['RFNode_ID'], self._node_to_srn_mapping[str(node['RFNode_ID'])],self._node_to_srn_mapping, self._gw_node_id, self._rf_scenario,self._traffic_scenario,self._traffic_log_path, self._global_start_time_obj,self._global_stop_time_obj,"FREEPLAY")
				node_instance.upload_to_cloud()
				#print("\n\t=======DRC Parsing COMPLETED=======\n")

		#return earlist time stamp
	def get_global_start_ts(self):
		p_grep = subprocess.Popen(['grep','-rw',self._traffic_log_path, "-e", "START Mgen"], stdout=subprocess.PIPE)
		p_cut1 = subprocess.Popen(["cut","-d",":","-f","2-"],stdin=p_grep.stdout, stdout=subprocess.PIPE)
		p_cut2 = subprocess.Popen(["cut","-d"," ","-f","1"], stdin=p_cut1.stdout, stdout=subprocess.PIPE)
		out, err = p_cut2.communicate()
		ts_list = out.splitlines()
		ts_obj_list = []
		for ts in ts_list:
			ts_obj_list.append( dt.strptime(ts, '%Y-%m-%d_%H:%M:%S.%f'))
		return min(ts_obj_list)
		#pprint.pprint(ts_list)

	def get_global_stop_ts(self):
		p_grep = subprocess.Popen(['grep','-rw',self._traffic_log_path, "-e", "STOP"], stdout=subprocess.PIPE)
		p_cut1 = subprocess.Popen(["cut","-d",":","-f","2-"],stdin=p_grep.stdout, stdout=subprocess.PIPE)
		p_cut2 = subprocess.Popen(["cut","-d"," ","-f","1"], stdin=p_cut1.stdout, stdout=subprocess.PIPE)
		out, err = p_cut2.communicate()
		ts_list = out.splitlines()
		ts_obj_list = []
		for ts in ts_list:
			ts_obj_list.append( dt.strptime(ts, '%Y-%m-%d_%H:%M:%S.%f'))
		return max(ts_obj_list)

def make_session(path):
	try:
		Freeplay_Session(path)
	except Exception as e:
		print(e)


if __name__ == "__main__":

		ts_now = dt.now()
		ts_offset = timedelta(days=1)#Number of days to consider match valid
		ts_cutoff = ts_now - ts_offset
		for reservation_path in os.listdir("/root/colosseum-nas/"):
			if fnmatch.fnmatch(reservation_path, "FREEPLAY-RESERVATION-*"):
				dir_stat = os.stat("/root/colosseum-nas/"+reservation_path)
				ts_dir_last_changed = dt.fromtimestamp(dir_stat.st_ctime)
				if ts_dir_last_changed > ts_cutoff:
					#print(reservation_path)
					#print(ts_dir_last_changed)
					abs_reservation_path = "/root/colosseum-nas/" + reservation_path	
					p = Process(target=make_session, args=(abs_reservation_path,))
					p.start()
					#print("Firing up process for {}".format(reservation_path))
		p.join()
