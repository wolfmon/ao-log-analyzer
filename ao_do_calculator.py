#!/usr/bin/env python2
from __future__ import generators
import os
import json
import re
import pprint
import subprocess
from datetime import datetime as dt
from datetime import timedelta
import fnmatch
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

from ao_mo_graphgen import Mandated_Outcomes_Scenario
from multiprocessing import Process
#Init for online plotting
plotly.tools.set_credentials_file(username='wolfmon', api_key='6jNlIoSmbfczYTihzLqI')


class MCHEM_Flow:
	def __init__(self,flow_id, drc_start_time, drc_stop_time , flow_on_time, flwo_off_time, deadline, time_steps):
		self._flow_id = flow_id
		self._drc_start_time = self.parse_timestamp(drc_start_time)
		self._drc_stop_time = self.parse_timestamp(drc_stop_time)
		self._flow_on_time = self.parse_timestamp(flow_on_time)
		self._flow_off_time = self.parse_timestamp(flwo_off_time)

		self._deadline = deadline
		self._packets = []
		self._throughput_slice = []
		self._time_steps = time_steps

	def get_cloud_trace(self):
		x =[]
		y =[]
		data = []
		for elem in self._packets:
			x.append(elem["timestamp"])
			y.append(elem["size"])
		trace = go.Scattergl(
			x = x,
			y = y,
			mode = 'lines+markers',
			name = str(self._flow_id)
			)
		return trace
		

		#This is getting stupid, change to increase preformance!!!
	def get_throughput_slice(self):
		tp_slice = [] 
		for elem in self._throughput_slice:
			tp_slice.append(elem["total_byte_count"])
		return tp_slice


	def process_slice(self):
		volatile_packet_list = self._packets

		delta_deadline = timedelta(seconds=self._deadline)
		ts_front = self._flow_on_time

		ts_end = self._flow_off_time

		
		while ts_front < ts_end:
			ts_back = ts_front + delta_deadline
			total_byte_count = 0
			for packet in volatile_packet_list:
				if ts_front < packet["timestamp"] < ts_back:
					if total_byte_count == 0:
						group_ts_start = packet["timestamp"] # mark the begining of the pack
					total_byte_count = total_byte_count + packet["size"]
					group_ts_end = packet["timestamp"] # mark the end of the pack, asssume drc file has sequential ts 

			self._throughput_slice.append({"total_byte_count":total_byte_count,"ts_group_front":group_ts_start,"ts_group_end":group_ts_end})
			ts_front = ts_back



	def get_relative_time_delta (self,ref_ts_str, target_ts_str):
		ref = self.parse_timestamp(ref_ts_str)
		tgt = self.parse_timestamp(target_ts_str)
		return (tgt - ref).total_seconds()

	def append_packet (self, timestamp, seq_num, size):

		self._packets.append({"timestamp":self.parse_timestamp(timestamp), "seq":int(seq_num), "size":int(size)})

	def parse_timestamp (self, ts_string):
		return (dt.strptime(ts_string, '%Y-%m-%d_%H:%M:%S.%f'))


	def get_total_period(self):
		time_delta = self._flow_off_time - self._flow_on_time
		return time_delta.total_seconds()

	def get_sum(self):
		total = 0
		for packet in self._packets:
			total = total + packet["size"]
		return total
	def get_avg_troughput(self):
		total = self.get_sum()
		time_delta = self._flow_off_time - self._flow_on_time
		return total / time_delta.total_seconds()

class Discrete_Mandate_Calculator(object):
	def __init__(self, dir_path, scenario_id, json_file_name_prefix, reservation_log_dir):
		self._mos = Mandated_Outcomes_Scenario(dir_path,scenario_id,json_file_name_prefix,reservation_log_dir)
		print("Scanning directories for [SCENARIO: {}] to populate Flow ID to DRC mapping, PLEASE WAIT...".format(scenario_id))
		self._flow_to_drc_mapping = self._mos.get_flow_to_drc_mapping()
		
		self._flow_to_ts_mapping = {int():[]}
		self._ts_flow_pair = self._mos.get_timestamp_flow_pair()
		for elem in self._ts_flow_pair:
			self._flow_to_ts_mapping.setdefault(elem[1],[])
			self._flow_to_ts_mapping[elem[1]].append(elem[0])
		#self._flow_to_ts_pair = {val: key for key, val in self._ts_flow_pair.items()}
		pprint.pprint(self._flow_to_drc_mapping)
		print("\nFlow ID to DRC mapping populated.")
		self._scenario_id = scenario_id
		self._ft_deadline = self._mos.get_flow_id_to_ft_deadline_mappings()
		self._cloud_traces = []
		#pprint.pprint(self._ft_deadline)

		pprint.pprint(self._flow_to_ts_mapping)


	def process_scenario(self):
		for flow in self._flow_to_drc_mapping:
			self.parse_flow(flow)

	def parse_flow(self,flow_id):
		print("\n====== Scenario ID: {} || Flow ID: {} || Deadline: {} second(s) || Unit in Byte(s) ======".format(self._scenario_id,flow_id,self._ft_deadline[flow_id]))
		drc_file_path = self._flow_to_drc_mapping[flow_id]
		flow_start_times = self._flow_to_ts_mapping[flow_id]
		drc_fd = open(drc_file_path, "r")
		lines = drc_fd.readlines()
		drc_fd.close()

		result = self.search_for_line(lines,'START Mgen')
		drc_start_time = re.split(' ',result[0])[0]

		result = self.search_for_line(lines,'STOP')
		drc_stop_time = re.split(' ',result[0])[0]

		result = self.search_for_line(lines,'ON flow>'+str(flow_id))
		on_timestamp = re.split(' ',result[0])[0]

		result = self.search_for_line(lines,'OFF flow>'+str(flow_id))
		off_timestamp = re.split(' ',result[0])[0]

		flow_object = MCHEM_Flow(flow_id, drc_start_time, drc_stop_time ,on_timestamp, off_timestamp,self._ft_deadline[flow_id],flow_start_times)

		print("\n====== Timing Information: ======\n\t DRC Start Time(START Mgen):{} \n\t Flow Start Time  (ON flow):{} ({} seconds since DRC Start) \n\t Flow Stop Time  (OFF flow):{} ({} seconds since DRC Start)\n".format(drc_start_time,on_timestamp,flow_object.get_relative_time_delta(drc_start_time,on_timestamp),off_timestamp,flow_object.get_relative_time_delta(drc_start_time,off_timestamp)))

		result = self.search_for_line(lines,'proto>UDP flow>'+str(flow_id))

		
		print("Stages specified in JSON:", flow_start_times)
		#pprint.pprint(result)

		for packet in result:
			ts = re.split(' ',packet)[0]
			seq = re.split(' |>',packet)[7]
			size = re.split(' |>',packet)[17]
			flow_object.append_packet(ts,seq,size)
		#print("Processing Slice...")
		flow_object.process_slice()

		#print("DONE!")
		total_slice = flow_object.get_throughput_slice()
		self._cloud_traces.append( flow_object.get_cloud_trace())
		try:
			print("\n---- Max Slice Size: {} | Min Slice Size: {} ----\n---- Total Period(ON->OFF): {} second(s) | Average Throughput: {} Byte(s)/s----\n".format(max(total_slice), min(total_slice),flow_object.get_total_period(),flow_object.get_avg_troughput()))
		except Exception as e:
			print("!!!!xxxxxx---Error Occured: {}\n, no usable dataflow, flow might be EMPTY or invalid!---xxxxxx!!!!".format(e))
		pprint.pprint(total_slice)
		#sarr = [str(a) for a in total_slice]
		#print(", ".join(sarr))
		#print('\nFlow: {} | Period: {} second(s) | Average Rate over entire period: {} Bps | Deadline: {} second(s)\n'.format(flow_id,flow_object.get_total_period(),flow_object.get_avg_troughput(), self._ft_deadline[flow_id]))

	def upload_to_cloud(self):
		try:
			py.plot(self._cloud_traces, filename ="SC2/MandatedOutcome/Scenario{}".format(self._scenario_id),auto_open=False)
		except Exception as e:
			print("Plot.ly API Error! Limit Reached", e)



	def search_for_line (self,lines, string_to_search):
		result = []
		for line in lines:
			if string_to_search in line:
				result.append(line)
		return result


def run(root, scenario, prefix, path):
		dm = Discrete_Mandate_Calculator("/root/scrimmage7_data/common/scenarios/",scenario,"Node1MandatedOutcomes_",scenario_to_reservation_mapping[scenario])
		dm.process_scenario()
		dm.upload_to_cloud()

def find_team_node_id(path, team_id):
	rtn = None
	for match_json_file in os.listdir(path):
			if fnmatch.fnmatch(match_json_file, '*.json'):
				_match_file_name = match_json_file
	_json_data_match = open(path + '/'+_match_file_name).read()
	_data_match = json.loads(_json_data_match)
	_json_node_data = _data_match['NodeData']
	for node in _json_node_data:
			if node['ImageName'] == team_id:
				rtn = node['RFNode_ID']
	return rtn

if __name__ == "__main__":
	
	scenario_to_reservation_mapping = {
	1157:"/root/scrimmage7_data/common/MATCH-004-RES-015825",
	2158:"/root/scrimmage7_data/common/MATCH-015-RES-015841",
	4158:"/root/scrimmage7_data/common/MATCH-026-RES-015857",
	5161:"/root/scrimmage7_data/common/MATCH-039-RES-015881",
	6161:"/root/scrimmage7_data/common/MATCH-051-RES-015898",
	}
	team_id = 'Team09'

	print(find_team_node_id(scenario_to_reservation_mapping[1157], 'Team09'))
	for scenario in scenario_to_reservation_mapping:
	# 	p = Process(target= run, args=("/root/scrimmage7_data/common/scenarios/",scenario,"Node1MandatedOutcomes_",scenario_to_reservation_mapping[scenario],))
	# 	print("Starting process for {}".format(scenario))
	# 	p.start()
	# p.join()
		node_id = find_team_node_id(scenario_to_reservation_mapping[scenario], team_id)
		if node_id is not None:
			dm = Discrete_Mandate_Calculator("/root/scrimmage7_data/common/scenarios/",scenario,"Node"+str(node_id)+"MandatedOutcomes_",scenario_to_reservation_mapping[scenario])
			dm.process_scenario()
		else:
			print("Error, no node_id found")
		#dm.upload_to_cloud()
    # dm = Discrete_Mandate_Calculator("/root/scenarios/",5058,"Node1MandatedOutcomes_","/root/batch-data/RESERVATION-64263")
    # dm.process_scenario()
    
    # #dm.parse_flow(5005)