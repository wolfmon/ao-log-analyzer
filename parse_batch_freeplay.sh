#!/bin/sh
rm /root/plotly/*

/root/colosseum-nas/pull_colosseum.sh
python /root/ao-log-parser/ao_parser_freeplay.py &
python /root/ao-log-parser/ao_parser_batch.py &
wait

echo "Last run: $(date)" > run.log

rm /var/www/html/*
cp /root/plotly/* /var/www/html/
#zip -r /var/www/html/plot.zip /root/plotly/
