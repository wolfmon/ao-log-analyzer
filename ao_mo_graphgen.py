#!/usr/bin/env python2
from __future__ import generators
import os
import json
import re
import pprint
import subprocess

'''
----------------------START OF ==Mandated_Outcomes_Scenario== CLASS------------------------
This class parses scneario JSON file and obtains the flow IDs that are of interest.

The helper functions defined in this class are mainly used for two purposes for now:
1. Provides mapping of flow id and their coorsponding file_transfer deadlines
2. Provides mapping of flow id and their time slot/stamp on which the demand starts.



'''

class Mandated_Outcomes_Scenario(object):

	def __init__(self, dir_path, scenario_id, json_file_name_prefix, reservation_log_dir):
		self._reservation_log_dir = reservation_log_dir # search path
		self._scenario_dir_path = dir_path
		self._scenario_id = scenario_id
		self._json_file_name_prefix = json_file_name_prefix
		self._json_path = self._scenario_dir_path + str(self._scenario_id) + "/Mandated_Outcomes/" + self._json_file_name_prefix + str(self._scenario_id)+".json"
		self._json_mo_data = open(self._json_path).read()
		self._mo_data = json.loads(self._json_mo_data)
		self._trpr_path = "/root/trpr2/trpr"
		self._mo_timestamp_flowid_pair =[]
		self._mo_flows = []
		self._mo_flows_no_duplicates = [] #list used for finding flows
		self._drc_dirs = []
		self._flow_id_to_ft_deadline_mapping = dict()
		
		self._flow_property_dictionary = dict()
		for array_elem in self._mo_data:
			for elem in array_elem["scenario_goals"]:
				# if "file_transfer_deadline_s" in elem["requirements"]:
				# 	# pprint.pprint(elem["flow_uid"])
				# 	# print("Timestamp "+ str(array_elem["timestamp"]))
				# 	self._mo_flows.append(elem["flow_uid"])
				# 	self._flow_id_to_ft_deadline_mapping.update({elem["flow_uid"]:elem["requirements"]["file_transfer_deadline_s"]})
				# 	self._mo_timestamp_flowid_pair.append((array_elem["timestamp"],elem["flow_uid"]))

				# 	'''
				# 	------------------------------------
				# 	Code added for 11/19 specification:
				# 	'''
				goal_set = '-'
				goal_type = '-'
				hold_period = -1
				max_latency = -1
				min_throughput = -1
				ft_deadline = -1
				ts = array_elem["timestamp"]
				try:
					goal_set = elem["goal_set"]
				except KeyError as e:
					print("Error while reading <goal_set>: {}".format(repr(e)))

				try:
					goal_type = elem["goal_type"]
				except KeyError as e:
					print("Error while reading <goal_type>: {}".format(repr(e)))

				try:
					hold_period = elem["hold_period"]
				except KeyError as e:
					print("Error while reading <hold_period>: {}".format(repr(e)))

				try:
					max_latency = elem["requirements"]["max_latency_s"]
				except KeyError as e:
					print("Error while reading <max_latency>: {}".format(repr(e)))

				try:
					min_throughput = elem["requirements"]["min_throughput_bps"]
				except KeyError as e:
					print("Error while reading <min_throughput>: {}".format(repr(e)))

				try:
					ft_deadline = elem["requirements"]["file_transfer_deadline_s"]
				except KeyError as e:
					print("Error while reading <file_transfer_deadline_s>: {}".format(repr(e)))


				self._flow_property_dictionary.update({elem["flow_uid"]:{"goal_set":goal_set,
					"goal_type":goal_type,"hold_period":hold_period,"max_latency":max_latency,
					"min_throughput":min_throughput,"ft_deadline":ft_deadline, "ts":ts}})

		self._mo_flows_no_duplicates = self.Remove(self._mo_flows)

	

	'''
	Returns the Flow IDs in containing discrete mandates
	'''
	def get_flow_ids(self):
		#return self._mo_flows
		return self._mo_flows_no_duplicates


		'''
			Find DRC file path given flow ID
			Return the file path containing the flow id
		'''

	def get_timestamp_flow_pair(self):
		return self._mo_timestamp_flowid_pair


	def get_flow_id_to_ft_deadline_mappings(self):
		return self._flow_id_to_ft_deadline_mapping

	'''
	Find first DRC that contains the flow ID
	'''
	def find_drcs(self, flows_id):
		rtn = subprocess.Popen(['grep','-rwl',self._reservation_log_dir,'-e','SEND proto>UDP flow>'+str(flows_id)],stdout=subprocess.PIPE)
		out, err = rtn.communicate()
		self._drc_dirs = out.splitlines()
		if out == '':
			return None
		else:
			return(self._drc_dirs[0])

	# Python code to remove duplicate elements 
	def Remove(self,duplicate): 
	    final_list = [] 
	    for num in duplicate: 
	        if num not in final_list: 
	            final_list.append(num) 
	    return final_list 

	def get_flow_to_drc_mapping(self):
		fid = self.get_flow_ids()
		flow_id_to_drcs_mapping = dict()
		for flow in fid:
			drc = self.find_drcs(flow)
			if drc != None:
				flow_id_to_drcs_mapping.update({flow:self.find_drcs(flow)})
		return flow_id_to_drcs_mapping


'''
----------------------END OF ==Mandated_Outcomes_Scenario== CLASS------------------------
'''

if __name__ == "__main__":
	
	#mos = Mandated_Outcomes_Scenario("/root/scenarios/",9502,"Node1MandatedOutcomes_","/root/freeplay-data/FREEPLAY-RESERVATION-61173")
	#mos = Mandated_Outcomes_Scenario("/root/scenarios/",8019,"Node1MandatedOutcomes_","/root/scrimmage6_data/common/MATCH-011-RES-014857")

	
	drc_walk_count = 0
	mos = Mandated_Outcomes_Scenario("/root/scenarios/",5058,"Node1MandatedOutcomes_","/root/batch-data/RESERVATION-64263")
	fid = mos.get_flow_ids()
	drcs = []
	for flow in fid:
		print("Walking all DRC files to find flow IDs...("+str(drc_walk_count) + ")")
		drc_walk_count = drc_walk_count + 1
		drc = mos.find_drcs(flow)
		if drc != None:
			drcs.append(mos.find_drcs(flow))
	drcs=mos.Remove(drcs)
	pprint.pprint(drcs)

