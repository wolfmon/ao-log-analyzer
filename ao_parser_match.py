#!/usr/bin/env python2
from __future__ import generators
import os
import json
import fnmatch
import re
import pprint
import subprocess
from datetime import datetime as dt
from datetime import timedelta
from multiprocessing import Process
from ao_parser_node import Srn_Node


class Match_Session(object):

	def __init__(self, match_dir, team_id = 'Team09'):
		#crete uninitilized insntance at creation
		self._session_dir_name = match_dir
		
		self._traffic_log_path = self._session_dir_name+'/traffic_logs'
		try:
			os.mkdir(self._traffic_log_path+'/'+"trpr_output")
		except OSError:
			pass

		self._match_file_name = 'uninitilized'
		self._team_id = team_id
		#Get the filename of Match_xxx.json
		for match_json_file in os.listdir(self._session_dir_name):
			if fnmatch.fnmatch(match_json_file, '*.json'):
				self._match_file_name = match_json_file

		#Parse Match_xxx.json into Python Runtime 
		#(TODO: Exception handling)
		self._json_data_match = open(self._session_dir_name + '/'+self._match_file_name).read()
		self._data_match = json.loads(self._json_data_match)

		#Parse match_conf.json into Python Runtime 
		#(TODO: Exception handling)
		self._json_mapping_data = open(self._session_dir_name+'/Inputs/match_conf.json').read()
		self._mapping_data = json.loads(self._json_mapping_data)
		self._node_to_srn_mapping = self._mapping_data['node_to_srn_mapping']

		#Grab useful data from the JSON input
		#TODO: handle key err exception
		self._batch_name = self._data_match['BatchName']
		self._rf_scenario_id = self._data_match['RFScenario']

		self._traffic_scenario_id = self._data_match['TrafficScenario']
		self._json_node_data = self._data_match['NodeData']
		self._gw_node_id = "NULL"
		#Obtained from srn_log from parent class
		#self._srn_list = []

		self._node_list = []
		#Obtained from mapping JSON file.
		self._global_start_time_obj = self.get_global_start_ts()

		for node in self._json_node_data:
			if node['ImageName'] == team_id:
				if(str(node['isGateway']) == str(True)):
					self._gw_node_id = node['RFNode_ID']
				node_instance = Srn_Node(node['RFNode_ID'], self._node_to_srn_mapping[str(node['RFNode_ID'])],self._node_to_srn_mapping, self._gw_node_id,self._rf_scenario_id,self._rf_scenario_id, self._traffic_log_path, self._global_start_time_obj,"scrimmage")
				node_instance.upload_to_cloud()
		print("\n\t=======DRC Parsing COMPLETED=======\n")



	def get_global_start_ts(self):
		p_grep = subprocess.Popen(['grep','-rw',self._traffic_log_path, "-e", "START Mgen"], stdout=subprocess.PIPE)
		p_cut1 = subprocess.Popen(["cut","-d",":","-f","2-"],stdin=p_grep.stdout, stdout=subprocess.PIPE)
		p_cut2 = subprocess.Popen(["cut","-d"," ","-f","1"], stdin=p_cut1.stdout, stdout=subprocess.PIPE)
		out, err = p_cut2.communicate()
		ts_list = out.splitlines()
		ts_obj_list = []
		for ts in ts_list:
			ts_obj_list.append( dt.strptime(ts, '%Y-%m-%d_%H:%M:%S.%f'))
		return min(ts_obj_list)

	def __str__(self):
		rtn = "\n    =========vv " + self._batch_name + " vv=========    \n\n\tRF Scenario: " + str(self._rf_scenario_id) +  "\n\tTraffic Scenario: " + str(self._traffic_scenario_id) + "\n"
		for elem in self._node_list:
			rtn = rtn + str(elem)		
		return rtn



class Scrimmage_Session(object):
	'''
	Initialization Function
	'''
	def __init__(self):
		self._team_id = "Team09"
		#Contains match session object
		self._match_sessions = []
		#Container for match dirs that we have participated in
		self._match_dirs = []
	'''
	Get all the match sessions that we have participated in.
	srn_log_path example: /root/scrimmage6_data/srn_logs/Team08
	Used to cross reference from common
	'''
	def obtain_useable_match_sessions(self,srn_log_path):
		self._match_dirs = os.listdir(srn_log_path)
	def run(self):
		self.obtain_useable_match_sessions("/root/scrimmage7_data/srn_logs/Team09")
		for match in self._match_dirs:
			#self.make_new_match_session(match)

			p = Process(target=self.make_new_match_session, args=(match,))
			p.start()
			print("Fired up process for {}".format(match))
		p.join()
		print ("COMPLETED")
		input()

	def make_new_match_session(self, match):
		print("Forking: {}\n".format(match))
		Match_Session("/root/scrimmage7_data/common/"+ match)
if __name__ == "__main__":
	#To Clean Up, remove all png and trpr_output directories
	#find . -name png -type d -exec rm -rf "{}" \;
	ss = Scrimmage_Session()
	ss.run()