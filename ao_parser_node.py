#!/usr/bin/env python2
from __future__ import generators
import os
import json
import fnmatch
import re
import pprint
import subprocess
from datetime import datetime as dt
from datetime import timedelta
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from multiprocessing.pool import ThreadPool
from multiprocessing import Pool
plotly.tools.set_credentials_file(username='wolfmon', api_key='6jNlIoSmbfczYTihzLqI')

'''
----------------------START OF ==SRN_Node== CLASS------------------------
Defines container cotaining a aggregate of Link_Performance, along with properties
of a SRN
Input:
		node_id : RF Node ID
		srn_id  : SRN ID
		node_to_srn_mapping : Mapping table used to reslove destation SRN
		is_gateway : Used for GW node Labeling
		traffic_log_path 	:	directory where /traffic_logs directory is located. 
'''
class Srn_Node (object):
	def __init__(self, node_id,srn_id ,node_to_srn_mapping, gateway_node_id,rf_scenario_id,traffic_scenario_id, traffic_log_path, global_start_time,global_stop_time, name_prefix):
		#RF/Traffic Node ID
		self._node_id = node_id
		#Dictionary Structure describing translation table between RF Node ID to SRN ID 
		self._node_to_srn_mapping = node_to_srn_mapping
		self._srn_id = srn_id
		self._gateway_node_id = gateway_node_id
		self._traffic_log_path = traffic_log_path
		self._traffic_scenario_id = traffic_scenario_id
		self._rf_scenario_id = rf_scenario_id
		#drc file naming scheme
		self._drc_search_arg = 'listen_SENDNODE-'+str(self._node_id)+'*'
		self._send_drc_search_arg= 'send_SENDNODE-'+str(self._node_id)+'*'
		self._name_prefix = name_prefix
		self._drc_dictionary= {}
		self.link_performance = []
		self._global_start_time = global_start_time
		self._global_stop_time = global_stop_time
		self._match_duration = (self._global_stop_time - self._global_start_time).total_seconds()
		
	def __str__(self):
		return "\nNode ID: "+ str(self._node_id) + "\tSRN ID: "+ str(self._srn_id) + "\t Gateway: " + str(self._is_gateway) + "\n"


	#Look for drc files that describe traffic going from this node to other nodes, output dest_to_drc_mapping dict.
	def search_for_drc(self, path = None, search_arg = None):
		#Contains a list of destnation nodes and their coorsponding drc file
		if path == None:
			path = self._traffic_log_path
		if search_arg == None:
			search_arg = self._drc_search_arg
		dest_to_drc_mapping = {}
		#Search for Mathing DRC file
		for drc_listen_file_name in os.listdir(path):
			if fnmatch.fnmatch(drc_listen_file_name, search_arg):
				if str(self._node_id) == re.split('_|-', drc_listen_file_name)[2]:
					self.rec_node_id = re.split('_|-', drc_listen_file_name)[4]
					dest_to_drc_mapping.update({self.rec_node_id:drc_listen_file_name})
		return dest_to_drc_mapping

	def parse_timestamp (self, ts_string):
		return (dt.strptime(ts_string, '%Y-%m-%d_%H:%M:%S.%f'))
		
	def get_relative_time_delta (self,ref_ts_str, target_ts_str):
		ref = self.parse_timestamp(ref_ts_str)
		tgt = self.parse_timestamp(target_ts_str)
		return (tgt - ref).total_seconds()

	def get_send_throughput_trace(self, drc_dictionary = None):
		if drc_dictionary == None : drc_dictionary == self._drc_dictionary
		traces = []
		for rec_node_id, drc_file_name in drc_dictionary.items():
			unique_flows, packet_list = self.parse_send_packets(drc_file_name)
			for flow in unique_flows:
				x_time = []
				y_size = []
				throughput_bins = [int(0)] * int(self._match_duration)
				for packet in packet_list:
					if str(packet["flow"]) == flow:
						#pkt_dict.update({(self.parse_timestamp(packet["timestamp"]) - self._global_start_time).total_seconds():packet["size"]})
						time_since_start = (self.parse_timestamp(packet["timestamp"]) - self._global_start_time).total_seconds()
						x_time.append(time_since_start)
						y_size.append(packet["size"])
						throughput_bins[int(time_since_start)] = throughput_bins[int(time_since_start)] + packet["size"]
				trace_total = go.Scattergl(
				x = x_time,
				y = y_size,
				legendgroup = 'flow-{}'.format(flow),
				mode = 'lines+markers',
				name = '{}-send-pkt_size_{}->{}(SRN:{})'.format(flow,self._node_id,rec_node_id,self._node_to_srn_mapping[rec_node_id]),
				line = dict(width = 1, dash = 'dot')
				#,showlegend = 'legendonly'
				)

				mo_data = self.get_mandated_outcome(self._rf_scenario_id,self._node_id,flow)
				trace_txt = ""
				for elem in mo_data:
					if "file_transfer_deadline_s" in elem["requirements"]:
						trace_txt = trace_txt+ ">Stage:{} {} {} Hold:{} FT Deadline:{} ".format(
							elem["ts_stage"], elem["goal_set"], elem["goal_type"], elem["hold_period"],elem["requirements"]["file_transfer_deadline_s"])
					else:
						trace_txt = trace_txt + ">Stage:{} {} {} Hold:{} Max LAT:{} Min TP:{} ".format(
							elem["ts_stage"], elem["goal_set"], elem["goal_type"], elem["hold_period"],elem["requirements"]["max_latency_s"],elem["requirements"]["min_throughput_bps"])

				trace_tp = go.Scattergl(
					x = range(0,int(self._match_duration)),
					y = throughput_bins,
					legendgroup = 'flow-{}'.format(flow),
					opacity = 1,
					text = trace_txt,
					fill='tozeroy',	
					#fillcolor = 'rgba(0,0,0,1)',
					mode = 'none',
					name = '{}-injected-tp_{}->{}(SRN:{})'.format(flow,self._node_id,rec_node_id,self._node_to_srn_mapping[rec_node_id]),
					line = dict(width = 1)
					)
				traces.append(trace_tp)
		return traces

	def get_listen_throughput_trace(self, drc_dictionary = None):
		if drc_dictionary == None : drc_dictionary == self._drc_dictionary
		#pool = ThreadPool(processes=45)
		traces = []
		for rec_node_id, drc_file_name in drc_dictionary.items():
			unique_flows, packet_list = self.parse_listen_packets(drc_file_name)
			for flow in unique_flows:
				x_time = []
				y_size = []
				throughput_bins = [int(0)] * int(self._match_duration)
				#pkt_dict = dict()
				for packet in packet_list:
					if str(packet["flow"]) == flow:
						time_since_start = (self.parse_timestamp(packet["timestamp"]) - self._global_start_time).total_seconds()
						x_time.append(time_since_start)
						y_size.append(packet["size"])
						throughput_bins[int(time_since_start)] = throughput_bins[int(time_since_start)] + packet["size"]

				trace_total = go.Scattergl(
				x = x_time,
				y = y_size,
				legendgroup = 'flow-{}'.format(flow),
				mode = 'markers',
				name = '{}-listen-pkt_size_{}->{}(SRN:{})'.format(flow,self._node_id,rec_node_id,self._node_to_srn_mapping[rec_node_id]),
				line = dict(width = 0.01,dash = 'dash')
				#,showlegend =  'legendonly'

				)
				mo_data = self.get_mandated_outcome(self._rf_scenario_id,self._node_id,flow)
				trace_txt = ""
				for elem in mo_data:
					if "file_transfer_deadline_s" in elem["requirements"]:
						trace_txt = trace_txt+ ">Stage:{} {} {} Hold:{} FT Deadline:{} ".format(
							elem["ts_stage"], elem["goal_set"], elem["goal_type"], elem["hold_period"],elem["requirements"]["file_transfer_deadline_s"])
					else:
						trace_txt = trace_txt + ">Stage:{} {} {} Hold:{} Max LAT:{} Min TP:{} ".format(
							elem["ts_stage"], elem["goal_set"], elem["goal_type"], elem["hold_period"],elem["requirements"]["max_latency_s"],elem["requirements"]["min_throughput_bps"])

				trace_tp = go.Scattergl(
					x = range(0,int(self._match_duration)),
					y = throughput_bins,
					legendgroup = 'flow-{}'.format(flow),
					mode = 'lines+markers',
					opacity = 0.7,
					text = trace_txt,
					fill='tozeroy',
					name = '{}-delivered-tp_{}->{}(SRN:{})'.format(flow,self._node_id,rec_node_id,self._node_to_srn_mapping[rec_node_id]),
					line = dict(width = 4)
					)
				traces.append(trace_tp)
		return traces

	def calculate_throughput (self, packet_dict):
		#print("calculating throughput")
		volatile_packet_dict = packet_dict
		rtn_ts = []
		rtn_tot_pkt_size = []
		ts_front = 0
		ts_step = 1

		for time_offset in xrange(0,1500):
			#print(time_offset)
			total_size = 0
			ts_back = ts_front +ts_step
			for ts, size in volatile_packet_dict.items():
				if ts > ts_front and ts < ts_back:
					total_size = total_size + size		
			rtn_ts.append(ts_front)
			rtn_tot_pkt_size.append(total_size)
			ts_front = ts_front+time_offset
		return rtn_ts , rtn_tot_pkt_size
	

	def get_listen_latency_trace(self, drc_dictionary = None):
		if drc_dictionary == None : drc_dictionary == self._drc_dictionary
		traces = []
		for rec_node_id, drc_file_name in drc_dictionary.items():
			unique_flows, packet_list = self.parse_listen_packets(drc_file_name)
			for flow in unique_flows:
				x_time = []
				y_latency = []
				for packet in packet_list:
					if str(packet["flow"]) == flow:
						x_time.append( (self.parse_timestamp(packet["timestamp"]) - self._global_start_time).total_seconds() )
						y_latency.append(self.get_relative_time_delta(packet["sent_ts"],packet["timestamp"]))
				
				#print("\n\n====\n\n Processing Scenario {} Node {} flow {}".format(self._rf_scenario_id,self._node_id,flow))
				mo_data = self.get_mandated_outcome(self._rf_scenario_id,self._node_id,flow)
				trace_txt = ""
				for elem in mo_data:
					if "file_transfer_deadline_s" in elem["requirements"]:
						trace_txt = trace_txt + ">Stage:{} {} {} Hold:{} FT Deadline:{} ".format(
							elem["ts_stage"], elem["goal_set"], elem["goal_type"], elem["hold_period"],elem["requirements"]["file_transfer_deadline_s"])
					else:
						trace_txt = trace_txt + ">Stage:{} {} {} Hold:{} Max LAT:{} Min TP:{} ".format(
							elem["ts_stage"], elem["goal_set"], elem["goal_type"], elem["hold_period"],elem["requirements"]["max_latency_s"],elem["requirements"]["min_throughput_bps"])
				trace = go.Scattergl(
				x = x_time,
				y = y_latency,
				legendgroup ='flow-{}'.format(flow),
				mode = 'markers',
				text = trace_txt,
				name = '{}-latency_{}->{}(SRN:{})'.format(flow,self._node_id,rec_node_id,self._node_to_srn_mapping[rec_node_id]),
				yaxis='y2')
				traces.append(trace)				
		return traces


	def parse_listen_packets(self, drc_path):
		packet_list = []
		unique_flows = []
		drc_fd = open(self._traffic_log_path+'/'+drc_path, "r")
		drc_lines = drc_fd.readlines()
		drc_fd.close()
		for line in drc_lines:
			if 'proto>UDP flow>' in line:
				ts = re.split(' ',line)[0]
				flow_id = re.split(' |>',line)[5]
				seq_num = re.split(' |>',line)[7]
				sent_ts = re.split(' |>',line)[17]
				size = re.split(' |>',line)[19]
				packet_list.append({"timestamp":ts, "flow":int(flow_id), "seq":int(seq_num),"sent_ts":sent_ts, "size":int(size)})
				if flow_id not in unique_flows:
					unique_flows.append(flow_id)
		return unique_flows, packet_list

	def parse_send_packets(self, drc_path):
		packet_list = []
		unique_flows = []
		drc_fd = open(self._traffic_log_path+'/'+drc_path, "r")
		drc_lines = drc_fd.readlines()
		drc_fd.close()
		for line in drc_lines:
			if 'proto>UDP flow>' in line:
				ts = re.split(' ',line)[0]
				flow_id = re.split(' |>',line)[5]
				seq_num = re.split(' |>',line)[7]				
				size = re.split(' |>',line)[17]
				packet_list.append({"timestamp":ts, "flow":int(flow_id), "seq":int(seq_num),"size":int(size)})
				if flow_id not in unique_flows:
					unique_flows.append(flow_id)
		return unique_flows, packet_list


	def get_mandated_outcome(self, scenario_id, node_id, flow_id):
		#Load JSON File
		scenario_dir_path = "/root/scenarios/"
		json_file_name_prefix = "Node"+str(self._node_id)+"MandatedOutcomes_"
		#Construct filepath
		json_file_path = scenario_dir_path + \
		str(self._rf_scenario_id) + \
		"/Mandated_Outcomes/" + \
		json_file_name_prefix + \
		str(self._rf_scenario_id) + \
		".json"
		json_mo_data = open(json_file_path).read()
		mo_data = json.loads(json_mo_data)
		mo_slice = []
		#Find flow ID
		for array_elem in mo_data:
			for elem in array_elem["scenario_goals"]:
				if elem["flow_uid"] == int(flow_id):
					elem.update({"ts_stage":array_elem["timestamp"]})
					mo_slice.append(elem)
		return mo_slice

	def upload_to_cloud(self):
		listen_drc_mapping = self.search_for_drc()
		send_drc_mapping = self.search_for_drc(search_arg = self._send_drc_search_arg)

		traces = self.get_listen_throughput_trace(drc_dictionary = listen_drc_mapping)
		traces = traces + self.get_listen_latency_trace(drc_dictionary = listen_drc_mapping)
		traces = traces + self.get_send_throughput_trace(drc_dictionary = send_drc_mapping)

		layout = go.Layout(

			hovermode = "closest",
			#plot_bgcolor='rgba(0,0,0,0.8)',
    		title=str(self._node_id) + "(SRN:" +str(self._srn_id) +")",
    		yaxis=dict(
        	title='Throughput (Bytes/sec)'
    					),
    		yaxis2=dict(
        	title='Latency (seconds)',
        	titlefont=dict(
        	    color='rgb(148, 103, 189)'
       					 ),
        	tickfont=dict(
            color='rgb(148, 103, 189)'
       					 ),
        	overlaying='y',
        	side='right'
   						 ),
    		legend = dict(
    			tracegroupgap = 25)
							)
		fig = go.Figure(data=traces, layout=layout)
		#print("Uploading Data to the Cloud...")
		print("Generating HTML for {}".format(re.split('/',self._traffic_log_path)[-2]))
		#py.plot(fig, filename ="SC2/{}/{}(RF:[{}]-TRAFFIC:[{}])/NODE-{}".format(self._name_prefix,re.split('/',self._traffic_log_path)[-2],self._rf_scenario_id,self._traffic_scenario_id,self._node_id),auto_open=False)
		plotly.offline.plot(fig, filename ="/root/plotly/{}(RF:[{}]-TRAFFIC:[{}])-NODE-{}.html".format(re.split('/',self._traffic_log_path)[-2],self._rf_scenario_id,self._traffic_scenario_id,self._node_id),auto_open=False)
		#print("Done")



'''
----------------------END OF ==SRN_Node== CLASS------------------------
'''

#==========Test Main()=============#

if __name__ == "__main__":
	print("\nStarting SRN_NODE Class Unit Test...\n")

	node_to_srn_mapping = {"1": 43, "2": 42, "3": 41, "4": 34, "5": 33, "6": 32, "7": 31, "8": 30, "9": 29, "10": 28, "11": 27, "12": 26, "13": 25, "14": 21, "15": 20, "16": 19, "17": 18, "18": 17, "19": 16, "20": 15, "21": 14, "22": 121, "23": 120, "24": 119, "25": 118, "26": 117, "27": 116, "28": 115, "29": 114, "30": 113, "31": 112, "32": 111, "33": 110, "34": 109, "35": 108, "36": 107, "37": 106, "38": 104, "39": 103, "40": 102, "41": 101, "42": 100, "43": 99, "44": 98, "45": 97, "46": 96, "47": 95, "48": 94, "49": 93, "50": 92, "51": 91, "52": 80, "53": 79, "54": 78, "55": 77, "56": 76, "57": 75, "58": 52, "59": 51, "60": 50}

	node_instance = Srn_Node(1,node_to_srn_mapping["1"],node_to_srn_mapping,True,"/root/scrimmage6_data/common/MATCH-078-RES-014999/traffic_logs")

	# mapping = node_instance.search_for_drc()
	# pprint.pprint(mapping)
	# node_instance.process_throughput(drc_dictionary = mapping, mode = 'png-1')
	# node_instance.process_latency(drc_dictionary = mapping, mode = 'png-1')

	#node_instance.process(mode = 'png-1-abs')
	#print("\n          ============================\n")
	#print(node_instance)

	#print("\nSRN_NODE Class Unit Test Finished.")
	node_instance.upload_to_cloud()


#=========END OF Test Main()=======#
